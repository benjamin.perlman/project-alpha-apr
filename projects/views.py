from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from django.contrib.auth.mixins import LoginRequiredMixin
from projects.models import Project


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(DetailView):
    model = Project
    template_name = "projects/detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):  # LoginRequiredMixin
    model = Project
    template_name = "projects/create.html"
    fields = [
        "name",
        "description",
        "members",
    ]

    success_url = reverse_lazy("home")

    def form_valid(self, form):

        item = form.save(commit=False)
        item.save()
        form.save_m2m()
        return redirect("show_project", item.pk)
