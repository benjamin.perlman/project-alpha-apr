from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from tasks.models import Task


class TaskCreateView(LoginRequiredMixin, CreateView):  # LoginRequiredMixin
    model = Task
    template_name = "tasks/create.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "project",
        "assignee",
    ]

    success_url = reverse_lazy("show_project")

    def get_success_url(self) -> str:
        return reverse_lazy("show_project", args=[self.object.id])

    def form_valid(self, form):
        item = form.save(commit=False)
        item.members = self.request.user
        item.save()

        return super().form_valid(form)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/mine.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/mine.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
